# [IDF Parking](www.parking.idfsistemas.com.br)

<center>
<a href="http://parking.idfsistemas.com.br" target="blank"><img src="./imgs/tela-site-framed.png"></a>
</center>

Gestão de estacionamentos do futuro

- Opere usando apenas um smartphone
- Sem custos de implantação
- Fácil e prático
- Acessivel de qualquer dispositivo Android

## IDF Parking - Web

IDF Parking possui uma plataforma Web onde é possível gerenciar os seus estacionamentos sem sair de casa ou de qualquer lugar onde tenha internet. Nela ficam centralizadas todas as configurações dos seus estacionamentos. Na plataforma Web você poderá:

<a href="https://parking.idfsistemas.com.br/app/login/" target="blank"><img src="./imgs/tela-web-framed.png" style="float: left; margin-right: 30px;"></a>

- Acessar os dados de todos os seus estacionamentos cadastrados
- Acompanhar o movimento do dia atual
- Emitir relatórios gerenciais
- Criar/alterar as regras de tarifação
- Cadastrar/Bloquear dispositivos (PDVs)
- Configurar impressoras de recibos

<p style="clear: both;"></p>

### Como acessar?

Acesse o endereço [parking.idfsistemas.com.br](https://parking.idfsistemas.com.br) e clique em **Login** no canto superior direito. Na tela de login entre com seu e-mail e senha cadastrados.
 
## IDF Parking - Mobile


O [IDF Parking - Mobile](https://play.google.com/store/apps/details?id=br.com.idfsistemas.parkingMobile) é um Sistema de PDV (_Ponto de Venda_) para estacionamentos. Com ele é possível registrar as entradas e saídas de veículos do estacionamento utilizando um ou mais Smartphones e Tablets. Para utiliza-lo é preciso ter um celular (ou tablet) e cadastrar o estacionamento no [IDF Parking - Web](parking.idfsistemas.com.br). _[ler mais...](./mobile/config-inicial)_

<a href="https://play.google.com/store/apps/details?id=br.com.idfsistemas.parkingMobile" target="blank"><img src="./imgs/tela-mobile-framed.png" style="float: left; margin-right: 30px;"></a>

**Funcionalidades do IDF Parking - Mobile:**

- Check-in/Checkout de veículo (mensalista e horista)
-  de veículo (mensalista e horista)
- Impressão de comprovante de entrada/saída
- Múltiplas regras de tarifação
- SUporte a uso de Múltiplos dispositivos simultaneos
- Relatórios administrativos (Disponibilizados no sistema Web)

### Instalando o IDF Parking - Mobile

Basta acessar a página de download do [IDF Parking - Mobile no Google Play](https://play.google.com/store/apps/details?id=br.com.idfsistemas.parkingMobile) e selecione a opção **Baixar/Instalar** _(Veja [Instalando o aplicativo](./mobile/config-inicial/#instalando-o-aplicativo) para detalhes)_.

<p style="clear: both;"></p>
