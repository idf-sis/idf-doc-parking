(function () {
	'use strict';
	$(document).ready(function () {
		// Solução paliativa para bug na busca do Read The Docs
		fixSearch();
	});

	/*
	 * RTD messes up MkDocs' search feature by tinkering with the search box defined in the theme, see
	 * https://github.com/rtfd/readthedocs.org/issues/1088. This function sets up a DOM4 MutationObserver
	 * to react to changes to the search form (triggered by RTD on doc ready). It then reverts everything
	 * the RTD JS code modified.
	 *
	 * Código baseado em:
	 *  https://github.com/nodemcu/nodemcu-firmware/commit/7dd89dd15ef993c9740ba4d4361e6be8edd1284b
	 */
	function fixSearch() {
		var target = document.getElementById('rtd-search-form');
		var config = {attributes: true, childList: true};

		var observer = new MutationObserver(function(mutations) {
			// if it isn't disconnected it'll loop infinitely because the observed element is modified
			observer.disconnect();
			var form = $('#rtd-search-form');
			form.empty();
			// window.location.hostname = "docs.idfsistemas.com.br"
			form.attr('action',
				'http://' + window.location.hostname +
				'/projects/parking/pt_BR/' +
				determineSelectedBranch() +
				'/search.html'
			);
			$('<input>').attr({
				type: "text",
				name: "q",
				placeholder: "Buscar na documentação"
			}).appendTo(form);
		});

		//window.location.origin = "http://docs.idfsistemas.com.br"
		if (window.location.origin.indexOf('idfsistemas.com.br') > -1) {
			observer.observe(target, config);
		}
	}
	/**
	* Analyzes the URL of the current page to find out what the selected GitHub branch is. It's usually
	* part of the location path. The code needs to distinguish between running MkDocs standalone
	* and docs served from RTD. If no valid branch could be determined 'dev' returned.
	*
	* Código baseado em:
	*  https://github.com/nodemcu/nodemcu-firmware/commit/7dd89dd15ef993c9740ba4d4361e6be8edd1284b
	*
	* @returns GitHub branch name
	*/
	function determineSelectedBranch() {
		// window.location.pathname = "/projects/parking/pt_BR/latest/"
		var branch = 'latest', path = window.location.pathname;
		if (window.location.origin.indexOf('idfsistemas.com.br') > -1) {
			// path is like /projects/parking/en/<branch>/<lang>/doc-page/ -> extract 'branch'
			// split[0] is an '' because the path starts with the separator
			var branchPathSegment = path.split('/')[4];
			// 'latest' is an alias on RTD for the 'dev' branch - which is the default for 'branch' here
			// if (branchPathSegment != 'latest') {
			branch = branchPathSegment;
			// }
		}
		return branch;
	}

}());
