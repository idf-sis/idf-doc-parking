/**
 * Componente baseado em:
 * http://www.w3schools.com/w3css/w3css_slideshow.asp
 */
function changeDivOfSlider(w3Slider, sum) {
	var slides= $(w3Slider).children('.w3-slide');
	var index = 0;

	// Ocultar o exibido atualmente
	slides.each(function(i, slide) {
		if ($(slide).css('display') === 'block') {
			index = i;
		}
		$(slide).css('display', 'none');
	});
	index = index + sum || 0;

	// Altera animacao de troca de imagem
    $(slides).removeClass('animate-left');
    $(slides).removeClass('animate-right');
    if (sum > 0) {
    	$(slides).addClass('animate-left');
    } else if (sum < 0) {
		$(slides).addClass('animate-right');
    }

	// Não passa do último
	$(w3Slider).children('.w3-slide-left').removeClass('w3-disabled');
	$(w3Slider).children('.w3-slide-right').removeClass('w3-disabled');
	if (index >= slides.length-1) {
		index = slides.length-1;
		$(w3Slider).children('.w3-slide-right').addClass('w3-disabled');
	} else if (index <= 0) {
		index = 0;
		$(w3Slider).children('.w3-slide-left').addClass('w3-disabled');
	}
    // Exibe solecionado
    $(slides[index]).css('display', 'block');
}

function initDivs() {
	$('.w3-slider').each(function(i, w3Slider) {
		$(w3Slider).addClass("w3-content");
		$(w3Slider).children('img').wrap(
			'<div class="w3-display-container w3-slide"></div>'
		);
		$(w3Slider).children('.w3-slide').each(function(i, slide) {
			$(slide).append(
				'<div class="w3-display-topleft w3-container w3-black">Passo '+
				(i+1)+':</div>'
			);
		});
		$(w3Slider).append(
			'<a class="w3-btn-floating w3-slide-left">❮</a>'
		);
		$(w3Slider).append(
			'<a class="w3-btn-floating w3-slide-right">❯</a>'
		);

		changeDivOfSlider(w3Slider);
	});
}

$(document).ready(function () {
	initDivs();

	$('.w3-slider > .w3-slide-left').on('click', function(element) {
		var w3Slider = $(this).parent('.w3-slider');
		changeDivOfSlider(w3Slider, -1);
	});

	$('.w3-slider > .w3-slide-right').on('click', function(element) {
		var w3Slider = $(this).parent('.w3-slider');
		changeDivOfSlider(w3Slider, 1);
	});
});
