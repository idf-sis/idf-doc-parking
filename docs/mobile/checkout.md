# O que é o Checkout

_Checkout_ é o nome dado a operação de **registro de saída de veículo** no estacionamento. Ele deve ser feito através de um celular ou tablet com o [IDF Parking - Mobile](https://play.google.com/store/apps/details?id=br.com.idfsistemas.parkingMobile) instalado e pareado com o estacionamento (_veja [Configurando Estacionamento](./config-inicial/#configurando-o-estacionamento)_ para mais detalhes).

Ao fazer o **Checkout** de um novo veículo o sistema irá:

1. Encerrar automaticamente a contagem de tempo de permanência desse veículo
2. Calcular o valor devido de acordo com a permanência e tipo do cliente e veículo
3. Imprimir um comprovante de saída contendo o tempo de permanência e o valor que deve ser pago (_veja [Configurando Impressora](./config-inicial/#configurando-a-impressora)_ para mais detalhes).

# Fazendo Checkout de veículos

<div class="w3-slider">
	<img src="../../imgs/mobile/checkout/01.jpg"/>
	<img src="../../imgs/mobile/checkout/02.jpg"/>
	<img src="../../imgs/mobile/checkout/03.jpg"/>
	<img src="../../imgs/mobile/checkout/04.jpg"/>
	<img src="../../imgs/mobile/checkout/05.jpg"/>
	<img src="../../imgs/mobile/checkout/06.jpg"/>
	<img src="../../imgs/mobile/checkout/07.jpg"/>
	<img src="../../imgs/mobile/checkout/08.jpg"/>
</div>

1. Selecione o veículo desejado
2. Será exibido os detalhes do veículo. Confira as informações e clique no botão **Checkout**
3. O sistema exibirá o formulário de conferência de checkout
4. Selecione a regra de tarifação aplicada
5. Selecione a forma de pagamento desejada pelo cliente
6. Confira o valor a ser pago no campo **Total** e clique no botão **Confirmar**
7. O comprovante de checkout será impresso automaticamente e será exibida uma tela confirmando a conclusão do checkout.
8. Clique no botão **encerrar** para retornar a tela inicial.

<p style="clear: both;"></p>

!!! note "Mensalista"
    Clientes **Mensalistas** não são cobrados no momento do checkout. Logo, não é necessário informar a regra de tarifação nem a forma de pagamento.

!!! note "Impressão automática"
    Para que o comprovante seja impresso é necessário que exista uma impressora configurada _(veja [Configurando Impressora](./config-inicial/#configurando-a-impressora) para mais detalhes)_.

!!! hint "Reimpressão"
    Caso ocorra erro na impressora no momento da impressão do comprovante ou seja necessária uma segunda via. É possível reimprimi-lo clicando no botão **Imprimir Recibo de Saída** (Passo 7).

# O comprovante de Checkout

Um comprovante de Checkout será impresso automaticamente ao realizar um checkout de veículo. Você pode desabilitar essa funcionalidade nas configurações do aplicativo se desejar. (_veja [Configurando Impressora](./config-inicial/#configurando-a-impressora)_ para mais detalhes).

É possível imprimir mais de uma via do comprovante de saída selecionando o botão **Imprimir recibo de Saída** exibido na tela de conclusão do Checkout. Você também poderá usar esse botão para reimprimir a primeira via caso ela não seja impressa por algum problema na impressora.
