# Tela inicial

O [IDF Parking - Mobile](https://play.google.com/store/apps/details?id=br.com.idfsistemas.parkingMobile) poderá ser iniciado em duas telas diferentes. São elas:

## Tela de cadastro de estacionamento

<img src="../../imgs/mobile/tela-inicial/01.png" style="float: left; padding-right: 50px;"/>

Na **primeira inicialização** do aplicativo será solicitada a configuração do estacionamento. Veja explicação da tela abaixo:

1. Nome do aplicativo
2. Botão para acesso ao menu do aplicativo
3. Mensagem alertando sobre a necessidade de configuração do estacionamento
4. Botão para iniciar a configuração do estacionamento

<p style="clear: both;"></p>

!!! note "Observação"
    _Veja [Configurando o Estacionamento](./config-inicial/#configurando-o-estacionamento) para detalhes sobre como configurar o estacionamento no dispositivo._

## Lista de veículos

Uma vez que o estacionamento estiver configurado. O sistema passará a utilizar a tela de lista de veículos como tela inicial. Veja abaixo explicação dos itens da tela:

<img src="../../imgs/mobile/tela-inicial/02.png" style="float: left; padding-right: 50px;"/>

1. Nome do estacionamento configurado
2. Botão para acesso ao menu do aplicativo
3. Lista de veículos no estacionamento. _Obs.: Clique em um dos veículos para ver detalhes ou realizar o **Checkout**_.
4. Etiqueta diferenciando veículo mensalista
5. Lupa para busca de veículos no estacionamento pela placa
6. Resumo de vagas disponíveis
7. Botão para iniciar **Check-in** de novo veículo


- **Ver detalhes de um veículo** clicando em um dos veículos da lista;
- **Buscar um veículo pela placa** clicando na ![Lupa](../imgs/lupa.png) _(Lupa)_ no canto superior da tela
- **Acessar o menu da aplicação** clicando no botão 
- **Fazer o check-in de um novo veículo** clicando no botão check-in no canto inferior direito da tela.

<p style="clear: both;"></p>
