# Sobre o Aplicativo

<a href="https://play.google.com/store/apps/details?id=br.com.idfsistemas.parkingMobile" target="blank"><img src="../../imgs/mobile/parking-icon_128.png" style="float: left; margin-right: 20px;"></a>

O [IDF Parking - Mobile](https://play.google.com/store/apps/details?id=br.com.idfsistemas.parkingMobile) é um Sistema de PDV (_Ponto de Venda_) para estacionamentos. Com ele é possível registrar as entradas e saídas de veículos do estacionamento utilizando um ou mais Smartphones e Tablets. Para utiliza-lo é preciso apenas ter um celular (ou tablet) Android e cadastrar o estacionamento no [IDF Parking - Web](parking.idfsistemas.com.br).

Ao instalar o [IDF Parking - Mobile](https://play.google.com/store/apps/details?id=br.com.idfsistemas.parkingMobile) em mais de um dispositivo e configurar todos em um mesmo estacionamento, as entradas e saídas realizadas em qualquer dispositivo dos dispositivos aparecerão em todos os outros!

**Funcionalidades do IDF Parking - Mobile:**

- Check-in/Checkout  de veículo 
- Suporte a mensalista e horista
- Impressão de comprovante de entrada
- Impressão de comprovante de saída/pagamento
- Suporte a múltiplas regras de tarifação (por tipo de veículo, convênios, e etc)
- Suporte a utilização ao mesmo tempo de mais de um dispositivo para lançamento de entradas e saídas
- Lista de veículos utilizando o estacionamento
- Relatórios administrativos (Disponibilizados no sistema Web)
- Compatível com Smartphones e Tablets Android


Para usar o aplicativo você precisará ter um Smartphone ou Tablet Android com o [IDF Parking - Mobile](https://play.google.com/store/apps/details?id=br.com.idfsistemas.parkingMobile) instalado, e uma conta no [IDF Parking - Web](parking.idfsistemas.com.br) com um ou mais estacionamentos cadastrados.

# Instalando o aplicativo

Para instalar o aplicativo no seu Dispositivo Android (Celuar ou Tablet) acesse a página de download do [IDF Parking - Mobile](https://play.google.com/store/apps/details?id=br.com.idfsistemas.parkingMobile) ou busque por "IDF Parking" no [Google Play](https://play.google.com). Uma vez que a página do _IDF Parking_ estiver aberta no Google Play do seu celular ou tablet, clique no botão **Instalar** e aguarde a instalação ser concluída.

Pronto! Agora basta abrir o **IDF Parking - Mobile** no seu celular ou tablet e [configurar o seu estacionamento](#configurando-o-estacionamento).

# Configurando o Estacionamento

<div class="w3-slider">
	<img src="../../imgs/mobile/config-estacionamento/01.jpg"/>
	<img src="../../imgs/mobile/config-estacionamento/02.jpg"/>
	<img src="../../imgs/mobile/config-estacionamento/03.jpg"/>
	<img src="../../imgs/mobile/config-estacionamento/04.jpg"/>
	<img src="../../imgs/mobile/config-estacionamento/05.jpg"/>
	<img src="../../imgs/mobile/config-estacionamento/06.jpg"/>
	<img src="../../imgs/mobile/config-estacionamento/07.jpg"/>
	<img src="../../imgs/mobile/config-estacionamento/08.jpg"/>
</div>

1. Abra o Menu Lateral clicando no botão ![Menu](../imgs/mobile/menu.png) no canto esquerdo superior da tela
2. Em seguida clique em **Configurações**
3. Selecione a opção **Estacionamento**
4. Clique no botão **Escanear**
5. Aponte a camera do aparelho para o QRCode do estacionamento
6. Será exibida uma mensagem avisando que pode ser necessário autorizar o dispositivo. Clique em **Prosseguir** para continuar.  
7. Uma nova mensagem será exibida. Clique em **Sim** para prosseguir.
8. Pronto, o dispositivo está pareado com o estacionamento.

<p style="clear: both;"></p>

!!! note "Observação"
    _Veja [Autorizando Dispositivo pareado](#autorizando-dispositivo-pareado) para detalhes sobre como autorizar o dispositivo pareado._

!!! attention "Atenção"
    Ao configurar um novo estacionamento em um aplicativo que já possui estacionamento a memória local do Aplicativo no dispositivo será apagada.

# Configurando a Impressora

Configure uma Impressora de Recibos no dispositivo para que os comprovantes de entrada/saída sejam impressos automáticamente ao serem realizados Check-in/Checkout de veículos pelo **IDF Parking - Mobile**. 

<div class="w3-slider">
	<img src="../../imgs/mobile/config-impressora/01.jpg" />
	<img src="../../imgs/mobile/config-impressora/02.jpg" />
	<img src="../../imgs/mobile/config-impressora/03.jpg" />
	<img src="../../imgs/mobile/config-impressora/04.jpg" />
	<img src="../../imgs/mobile/config-impressora/05.jpg" />
	<img src="../../imgs/mobile/config-impressora/06.jpg" />
</div>

1. Abra o Menu Lateral clicando no botão ![Menu](../imgs/mobile/menu.png) no canto esquerdo superior da tela
2. Em seguida clique em **Configurações**
3. Selecione a opção **Impressão de Recibos**
4. Clique no botão **Impressora**
5. Selecione uma das impressoras da lista de impressoras disponíveis e clique em **OK**
6. Pronto! A impressora de recibos está configurada e pronta para uso.

<p style="clear: both;"></p>

!!! note "Observação"
    As impressoras precisam ser previamente configurada no estacionamento pelo _IDF Parking - Web_. _Veja [Cadastrando Impressora no Estacionamento](#cadastrando-nova-impressora-no-estacionamento) para mais detalhes._ 

!!! tip "Dica"
    Você pode desmarcar a opção de **Impressão automática** _(Tela do Passo 4)_ se não desejar que os recibos de entrada/saída sejam impressos automáticamente.

    Ao fazer isso ainda será possível imprimir o recibo clicando no botão de impressão disponivel nas telas de Check-in/Checkout.
