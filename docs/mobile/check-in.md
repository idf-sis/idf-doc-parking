# O que é o Check-in

_Check-in_ é o nome dado a operação de **registro de entrada de veículo** no estacionamento. Ele deve ser feito através de um celular ou tablet com o [IDF Parking - Mobile](https://play.google.com/store/apps/details?id=br.com.idfsistemas.parkingMobile) instalado e pareado com o estacionamento (_veja [Configurando Estacionamento](./config-inicial/#configurando-o-estacionamento)_ para mais detalhes).

Ao fazer o **Check-in** de um novo veículo o sistema iniciará automaticamente a contagem de tempo de permanência desse veículo e imprimirá um comprovante de entrada (_veja [Configurando Impressora](./config-inicial/#configurando-a-impressora)_ para mais detalhes).

Quando o veículo sair do estacionamento deverá ser realizado o Checkout do mesmo. Nesse momento o sistema irá calcular o valor devido e imprimirá o comprovante de saída do veículo. (_veja [O que é o Checkout](./checkout/#o-que-e-o-checkout)_ para mais detalhes).

# Fazendo Check-in de veículos

<div class="w3-slider">
	<img src="../../imgs/mobile/checkin/01.jpg"/>
	<img src="../../imgs/mobile/checkin/02.jpg"/>
	<img src="../../imgs/mobile/checkin/03.jpg"/>
	<img src="../../imgs/mobile/checkin/04.jpg"/>
	<img src="../../imgs/mobile/checkin/05.jpg"/>
</div>

1. Selecione o botão **Checkin** no canto inferior direito da tela
2. Será exibido o fomulário de Checkin
3. Preencha o formulário com os dados do veículo e clique em **Confirmar** (_Obs.: Somente os campos **placa** e **mensalista** são obrigatórios_)
4. Será exibida uma tela confirmando a conclusão do Checkin e impresso o comprovante de entrada do veículo. Clique no botão **Encerrar** para voltar a tela inicial
5. Pronto, check-in concluído! O novo veículo será listado na tela inicial.

<p style="clear: both;"></p>

!!! note "Mensalista"
    Para clientes mensalista basta marcar o opção **Mensalista** no formulário de Check-in (passo 3).

!!! note "Impressão automática"
    Para que o comprovante seja impresso é necessário que exista uma impressora configurada _(veja [Configurando Impressora](./config-inicial/#configurando-a-impressora) para mais detalhes)_.

!!! hint "Reimpressão"
    Caso ocorra erro na impressora no momento da impressão do comprovante ou seja necessária uma segunda via. É possível reimprimi-lo clicando no botão **Imprimir Recibo de Saída** (Passo 4).

## O comprovante de Check-in

Um comprovante de Check-in será impresso automaticamente ao realizar um check-in de veículo. Você pode desabilitar essa funcionalidade nas configurações do aplicativo se desejar. (_veja [Configurando Impressora](./config-inicial/#configurando-a-impressora)_ para mais detalhes).

É possível imprimir mais de uma via do comprovante de entrada selecionando o botão **Imprimir recibo de Entrada** exibido na tela de conclusão do Check-in. Você também poderá usar esse botão para reimprimir a primeira via caso ela não seja impressa por algum problema na impressora.
