# Sobre Estacionamentos

Com o IDF Parking você poderá gerenciar todos os seus estacionamentos em um único sistema e de qualquer lugar onde estiver desde que tenha acesso a internet. Você só precisa cadastrar os seus estacionamentos no sistema e configurar os pontos de venda em celulares ou tablets. E pronto! Registre as entradas e saídas dos veículos no celular ou tablet configurado e as vendas realizadas por eles serão automaticamente lançadas no estacionamento correto gerando relatórios separados por cada um dos seus estacionamentos.

# Listando Estacionamentos cadastrados

1. Acesse parking.idfsistemas.com.br e clique em **login** no canto superior direito da tela
2. Informe seu e-mail e senha de cadastro e clique em **Acessar**
3. Sua conta será carrega mostrando na tela inicial a lista dos estacionamentos cadastrados na sua conta
4. Clique em um dos estacionamentos listados para acessar o [Painel Administrativo do estacionamento](#painel-administrativo-do-estacionamento) e ter acesso a configuração de dispositivos do estacionamento, visualização de relatórios, e muito mais...

!!! note "Observação"
    No primeiro acesso você não possuirá estacionamentos cadastrado. _Veja [Cadastrando Novo Estacionamento](#cadastrando-novo-estacionamento) para mais detalhes sobre como cadastrar o seu primeiro estacionamento._

## Painel Administrativo do Estacionamento

No Painel Administrativo do Estacionamento é exibido um **resumo** das principais informações relativas ao estacionamento. É também através dele que você terá acesso a toda a configuração e relatórios do estacionamento.

As principais informações disponibilizados no Painel Administrativos do Estacionamento são:

### Resumo do Dia

| Painel | Descrição |
| ------ | --------- |
| ![Horista](../imgs/estacionamento/horista.png) | **Horistas:** Exibe a quantidade de clientes horistas que fizeram checkout do estacionamento no dia |
| ![Mensalista](../imgs/estacionamento/mensalista.png) | **Mensalistas:** Exibe a quantidade de clientes mensalistas que fizeram checkout do estacionamento no dia |
| ![Total](../imgs/estacionamento/total.png) | **Total:** Exibe o total de carros que fizeram checkout do estacionamento no dia |
| ![Faturamento](../imgs/estacionamento/faturamento.png) | **Faturamento:** Exibe o faturamento bruto total do dia atual |

### Configurações do estacionamento

| Painel | Descrição |
| ------ | --------- |
| ![Dispositivos](../imgs/estacionamento/dispositivos.png) | **Dispositivos configurados:** Lista resumida dos dispositivos configurados como PDVs (Pontos de Venda) do estacionamento |
| ![Regras](../imgs/estacionamento/regras.png) | **Regras de tarifação ativas:** Lista resumida das Regras de Tarifação ativas no estacionamento |
| ![Impressoras](../imgs/estacionamento/impressoras.png) | **Impressoras configuradas:** Lista resumida das impressoras de recibo configuradas para o estacionamento |


# Cadastrando novo Estacionamento

1. Acesse a lista de estacionamentos _(Veja [Listando Estacionamentos cadastrados](#listando-estacionamentos-cadastrados) para mais detalhes)_
2. Clique no botão ![mais](../imgs/plus.png) _(mais)_ no canto inferior direito da lista de estacionamentos
3. Informe os dados do estacionamento e clique no botão **Salvar**
4. Pronto! O novo estacionamento será salvo e a lista atualizada dos seus estacionamentos será exibida.
5. Clique no novo estacionamento para ter acesso a configuração dos dispositivos do estacionamento, visualização de relatórios, e muito mais...


# Editando Estacionamento existente

1. Acesse a lista de estacionamentos _(Veja [Listando Estacionamentos cadastrados](#listando-estacionamentos-cadastrados) para mais detalhes)_
2. Clique no estacionamento desejado
3. Será exibida o painel de administração do estacionamento selecionado
4. Clique no botão ![lápis](../imgs/pencil.png) _(lápis)_ no canto direito superior da tela
5. Altere as informações desejadas e clique no botão **Salvar**
6. Pronto! Suas alterações nos dados cadastrais do estacionamento foram salvas.

# Removendo Estacionamento existente

1. Acesse a lista de estacionamentos _(Veja [Listando Estacionamentos cadastrados](#listando-estacionamentos-cadastrados) para mais detalhes)_
2. Clique no estacionamento desejado
3. Será exibida o painel de administração do estacionamento selecionado
4. Clique no botão ![lixeira](../imgs/trash.png) _(lixeira)_ no canto direito superior da tela
5. Será exibida uma mensagem de confirmação. Leia a mensagem e clique no botão **Sim** para remover o estacionamento
6. Pronto! O estacionamento foi removido..

!!! attention "Atenção"
    Ao remover um estacionamento todos os dados relacionados ao estacionamento serão **excluídos definitivamente** e não poderão ser recuperados.
