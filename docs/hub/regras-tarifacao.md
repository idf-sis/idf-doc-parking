# Sobre Regra de Tarifação

Um Regra de Tarifação define qual preço deve ser cobrado pelo uso do estacionamento. Cada estacionamento poderá possuir mais de uma Regra de Tarifação sendo que cada uma delas deverá definir uma [configuração completa](#configurando-a-regra-de-tarifacao) capaz de especificar a variação do preço cobrado de acordo com o tempo de uso do estacionamento.

# Listando Regras de Tarifação do estacionamento

1. Acesse o _[Painel Administrativo do Estacionamento](./estacionamentos/#painel-administrativo-do-estacionamento)_ desejado. _(Veja [Listando Estacionamentos cadastrados](./estacionamentos/#listando-estacionamentos-cadastrados) para mais detalhes)_
2. No painel de **Regras de tarifação ativas** será exibida algumas das regras de tarfição ativas no estacionamento.
3. Clique no botão ![listar](../imgs/list.png) _(Listar)_ ou em _**Ver mais**_ para listar todas as regras de tarifação do estacionamento.
4. Selecione uma das regras listadas para ver os detalhes da regra.

# Cadastrando nova Regra de Tarifação

1. Acesse o _Painel Administrativo_ do estacionamento desejado _(Veja [Listando Estacionamentos cadastrados](./estacionamentos/#listando-estacionamentos-cadastrados) para mais detalhes)_
2. No painel de **Regras de tarifação ativas** será exibida algumas das regras de tarfição ativas no estacionamento.
3. Clique no botão ![cadastrar](../imgs/plus2.png) _(Mais)_
4. Será exibido o formulário de cadastro de regra de tarifação. Preencha o formulário com os dados desejados e clique no botão _**Salvar**_. (_Veja [Configurando a Regra de Tarifação](#configurando-a-regra-de-tarifacao) para mais detalhes sobre como preencher o formulário_)
5. Pronto! A nova regra de tarifação está salva e pronta para ser utilizada no seu estacionamento.

# Alterando Regra de Tarifação existente

1. Acesse a **Lista de Regras de tarifação**. _(Veja [Listando Regras de Tarifação do estacionamento](#listando-regras-de-tarifacao-do-estacionamento) para mais detalhes)_
2. Selecione a regra de tarifação que deseja alterar.
3. Será exibido o formulário para alterar os dados de configuração da regra de tarifação.
4. Altere as informações desejadas e clique no botão _**Salvar**_. (_Veja [Configurando a Regra de Tarifação](#configurando-a-regra-de-tarifacao) para mais detalhes sobre como preencher o formulário_)
5. Pronto! As alterações na regra de tarifação foram salvas.

## Configurando a Regra de Tarifação

### Campos Configuráveis

<table>
	<tr><th colspan="2">Horista</th></tr>
	<tr>
		<th>Fração (minutos)</th>
		<td>
			Fração mínima de tempo cobrada pelo estacionamento.<br>
			<em><u>Exemplo:</u> Se o veículo ficar por <b>1 hora e 22 minutos</b> em um estacionamento com <b>fração: 15min</b>. Será cobrado o valor correspondente a <b>1 hora e 30 minutos</b> (6 frações de 15 minutos).</em>
		</td>
	</tr>
	<tr>
		<th>Valor unitário (R$)</th>
		<td>
			Valor em reais cobrado <b>por cada hora</b> de uso do estacionamento.<br>
			<em><u>Exemplo:</u> Se o veículo ficar por <b>45 minutos</b> em um estacionamento com <b>fração: 15min</b> e <b>valor unitário: R$ 8,00</b>. O valor cobrado será de <b>R$ 6,00</b></em>
		</td>
	</tr>
	<tr>
		<th>Valor mínimo (R$)</th>
		<td>
			Valor mínimo em reais cobrado do cliente. Se informado e o resultado do cálculo de preço a ser cobrado pelas frações utilizadas for inferior ao valor mínimo. Então o valor mínimo será cobrado em vez do valor das frações utilizadas.<br>
			<em><u>Exemplo:</u> Se o veículo ficar por <b>30 min</b> em um estacionamento com <b>fração=15min</b>, <b>valor unitário=R$ 8,00</b> e <b>valor mínimo=R$ 8,00</b>. O valor cobrado será de <b>R$ 8,00</b>.</em>
		</td>
	</tr>
	<tr>
		<th>Valor máximo (R$)</th>
		<td>
			Valor máximo em reais cobrado do cliente. Se informado e o resultado do cálculo de preço a ser cobrado pelas frações utilizadas for superior ao valor máximo. Então o valor máximo será cobrado em vez do valor das frações utilizadas.<br>
			<em><u>Exemplo:</u> Se o veículo ficar por <b>8 horas</b> em um estacionamento com <b>fração=15min</b>, <b>valor unitário=R$ 8,00</b> e <b>valor máximo=R$ 32,00</b>. O valor cobrado será de <b>R$ 32,00</b>.</em>
		</td>
	</tr>
	<tr><th colspan="2">Dia (diária com pernoite)</th></tr>
	<tr>
		<th>Valor unitário (R$)</th>
		<td>
			Valor fixo cobrado pela diária no estacionamento. Esse valor será cobrado caso o cliente deixe o veículo no estacionamento por 24h ou mais, mesmo que ultrapasse o valor máximo definido.<br>
			<em><u>Exemplo:</u> Se o veículo ficar por <b>32 horas</b> em um estacionamento com <b>fração=15min</b>, <b>valor unitário=R$ 8,00</b>, <b>valor máximo=R$ 32,00</b> e <b>Diária=R$ 60,00</b> (não fracionável). O valor cobrado será de <b>R$ 60,00</b>.</em>
		</td>
	</tr>
	<tr>
		<th>Fracionar (Sim/Não)</th>
		<td>
			Define se a quantidade de tempo que ultrapassar as 24h deve ser tarifada de acordo com a configuração de tarifação para horista.<br>
			<em><u>Exemplo 1:</u> Se o veículo ficar por <b>50 horas</b> em um estacionamento com <b>fração=15min</b>, <b>valor unitário=R$ 8,00</b>, e <b>Diária=R$ 60,00</b> <b>Fracionável</b>. O valor cobrado será de <b>R$ 136,00</b> (Duas diárias mais 2 horas)</em><br>
			<em><u>Exemplo 2:</u> Se o veículo ficar por <b>50 horas</b> em um estacionamento com <b>fração=15min</b>, <b>valor unitário=R$ 8,00</b>, e <b>Diária=R$ 60,00</b> <b>Não fracionável</b>. O valor cobrado será de <b>R$ 120,00</b> (Duas diárias)</em>
		</td>
	</tr>
</table>

### Exemplo de Regras de Tarifação

Considerando que um estacionamento tenha a seguinte tabela de cobrança:

|     Permanência     | Carro    | Moto    | Cliente Supermercado
| ------------------- | -------- | ------- | --------------------
| **0:00 h - 0:30 h** |  R$ 4,00 | R$ 2,00 | R$ 2,00
| **0:30 h - 1:00 h** |  R$ 4,00 | R$ 2,50 | R$ 2,00
| **1:00 h - 1:30 h** |  R$ 6,00 | R$ 3,75 | R$ 2,00
| **1:30 h - 2:00 h** |  R$ 8,00 | R$ 5,00 | R$ 2,00
| **2:00 h - 2:30 h** | R$ 10,00 | R$ 6,25 | R$ 2,00
| **2:30 h - 3:00 h** | R$ 12,00 | R$ 7,50 | R$ 2,00
| **3:00 h - 3:30 h** | R$ 14,00 | R$ 8,75 | R$ 2,00
| **3:30 h - 4:00 h** | R$ 16,00 | R$10,00 | R$ 2,00
| Acima de 4 horas    | R$ 16,00 | R$10,00 | R$ 2,00
| Diária com pernoite | R$ 30,00 | R$15,00 | R$15,00

As seguintes regras de tarifação devem criadas no sistema:

|                     Regra: | Carro    | Moto    | Cliente Supermercado
| --------------------------:| -------- | ------- | --------------------
| **Fração:**                | 30min    | 30min   | -
| **Valor unitário:**        | R$ 4,00  | R$ 2,50 | -
| **Valor mínimo:**          | R$ 4,00  | R$ 2,00 | -
| **Valor máximo:**          | R$ 16,00 | R$ 10,00| R$ 2,00
| **Valor unitário diária:** | R$ 30,00 | R$ 15,00| R$ 15,00


# Removendo Regra de Tarifação existente

`Funcionalidade indisponível no momento`
