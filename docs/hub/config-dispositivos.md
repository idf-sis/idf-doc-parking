# Sobre Dispositivos

Os dispositivos do estacionamento são os Celulares ou Tablets configurados para registrar as entradas e saídas de veículos no estacionamento. É possível configurar mais de um Dispositivo para cada estacionamento.

Para configurar um Celular ou Tablet como um dispositivo do estacionamento você precisará instalar o aplicativo <a href="http://play.google.com/store/apps/details?id=br.com.idfsistemas.parkingMobile" target="_blank">IDF Parking - Mobile</a> no aparelho e [Pareá-lo](#pareando-novo-dispositivo) com o estacionamento desejado.

# Pareando novo Dispositivo

Para parear um dispositivo Android com o estacionamento você precisará ter o aplicativo <a href="http://play.google.com/store/apps/details?id=br.com.idfsistemas.parkingMobile" target="_blank">IDF Parking - Mobile</a> instalado no aparelho. Uma vez que o aplicativo for instalado, siga os passos baixos:

1. **IDF Parking - Web:** Acesse o Painel Administrativo do estacionamento desejado (_Ver [Painel Administrativo do estacionamento](./estacionamento/#painel-administrativo-do-estacionamento) para mais detalhes_)
2. **IDF Parking - Web:** Clique no botão ![Mais](../imgs/plus2.png) (_Mais_) do painel de **Dispositivos Configurados**
4. **IDF Parking - Web:** Será exibida uma imagem com um QRCode na tela que deverá ser lido pelo parelho celular ou tablet utilizando o _IDF Parking - Mobile_
5. **IDF Parking - Mobile:** Abra o aplicativo no aparelho
6. **IDF Parking - Mobile:** Abra o Menu (_Botão ![Menu](../imgs/mobile/menu.png) no canto esquerdo superior da tela_)
7. **IDF Parking - Mobile:** Em seguida clique em **Configurações**
7. **IDF Parking - Mobile:** Selecione a opção **Estacionamento** e clique no botão **Escanear**
8. **IDF Parking - Mobile:** Aponte a camera do aparelho para o QRCode exibido no _passo 4_
9. **IDF Parking - Mobile:** Após pareado o aparelho ficará _Aguardando a autorização de Uso_.  
_Veja [Autorizando Dispositivo pareado](#autorizando-dispositivo-pareado) para detalhes sobre como autorizar o dispositivo._

# Listando Dispositivos do estacionamento

1. Acesse o _[Painel Administrativo do Estacionamento](./estacionamentos/#painel-administrativo-do-estacionamento)_ desejado. _(Veja [Listando Estacionamentos cadastrados](./estacionamentos/#listando-estacionamentos-cadastrados) para mais detalhes)_
2. No painel de **Dispositivos configurados** será exibido alguns dos dispositivos configurados.
3. Clique no botão ![listar](../imgs/list.png) _(Listar)_ ou em _**Ver mais**_ para listar todas os dispositivos configurados.
4. Selecione um dos dispositivos listados para ver os detalhes do dispositivo.

# Autorizando Dispositivo pareado

1. Acesse o dispositivo desejado _(Veja [Listando Dispositivos do estacionamento](#listando-dispositivos-do-estacionamento) para mais detalhes)_
2. No painel de **Informações Complementares** marque a opção **Autorizado** e clique no botão **Salvar**.

**Pronto!** O Seu aparelho está pareado e autorizado para registrar entradas e saídas de veículos no estacionamento.

# Removendo Dispositivo pareado

`Funcionalidade indisponível no momento`



