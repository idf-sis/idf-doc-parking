# O Módulo de relatórios

Além das informações resumidas disponibilizadas no [Painel Administrativo](../estacionamentos.md/#painel-administrativo-do-estacionamento) de Cada Estacionamento. Para garantir o acompanhamento do histórico de uso e faturamento dos seus estacionamentos _IDF Parking_ disponibiliza na sua interface Web um conjunto de relatórios administrativos.

## Veículos avulsos por valor pago

Relatório contendo o número de veículos que utilizam o estacionamento em um período determinado agrupado pelo valor pago por cada veículo. Utilize esse relatório para visualizar o valor mais frequentemente cobrado dos clientes do seu estacionamento.

## Faturamento

Relatório contendo de número e o total cobrado dos veículos que usaramo o estacionamento em um determinado período agrupado por dia ou por mês. Utilize esse relatório para acompanhar o faturamento diário ou mensal do estacionamento.

## Permanência

Exibe a permanência média dos veículos que usaram o estacionamento em um determinado período estratificada pelo número de horas e tipo de cliente (Horista/Mensalista). Utilize esse relatório para avaliar o perfil dos clientes do estabelecimento verificando por quanto tempo os veículos estão utilizando o estacionamento. 

## Movimento por Período

Exibe o detalhamento de todos os Checkouts realizados em um determinado período. Inclui informações de placa, Data entrada, Período de Permanência, Tipo de cobrança, e valo cobrado. Utilize esse relatório para acompanhar de forma detalhada toda a movimentação de saídas de veículos do estacionamento.
