# Sobre Impressoras

As Impressoras do estacionamento são as impressoras utilizadas para realizar a impressão dos comprovantes de entrada/saída do estacionamento.

Para evitar o retrabalho de configurar em cada dispositivo do estacionamento uma mesma impressora. O IDF Parking centraliza as configurações das impressoras do estacionamento no _IDF Parking - Web_. Dessa forma, após a impressora ser configurada através da interface do _IDF Parking - Web_, ela passará a ser disponibilizada para seleção em todos os dispositivos ativos do estacionamento.

Cada impressora configurada poderá ser utilizada por mais de um dispositivo. E ainda é possível configurar mais de uma Impressora para um mesmo estacionamento.

# Listando Impressoras do estacionamento

1. Acesse o _[Painel Administrativo do Estacionamento](./estacionamentos/#painel-administrativo-do-estacionamento)_ desejado. _(Veja [Listando Estacionamentos cadastrados](./estacionamentos/#listando-estacionamentos-cadastrados) para mais detalhes)_
2. No painel de **Impressoras configurados** serão exibidas algumas das impressoras do estacionamento.
3. Clique no botão ![listar](../imgs/list.png) _(Listar)_ ou em _**Ver mais**_ para listar todas as impressoras do estacionamento.
4. Selecione uma das impressoras listadas para ver os detalhes da impressora.

# Cadastrando nova Impressora no estacionamento

1. Acesse o Painel Administrativo do estacionamento desejado (_Ver [Painel Administrativo do estacionamento](./estacionamento/#painel-administrativo-do-estacionamento) para mais detalhes_)
2. Clique no botão ![Mais](../imgs/plus2.png) (_Mais_) do painel de **Impressoras Configuradas**
3. Será exibido o formulário de cadastro de impressoras
4. Preencha os campos do formulário e clique no botão **Salvar** (_Ver [Tipos de Impressoras suportadas](#tipos-de-impressoras-suportadas) para mais detalhes_)
5. Pronto! A impressora está configurada e pronta para ser utilizada nos dispositivos pareados com o estabelecimento

## Tipos de Impressoras suportadas

Ao cadastrar uma nova impressora no estacionamento você precisará informar o tipo dessa impressora. Para cada tipo de impressora uma configuração diferente será exigida para que ela funcione corretamente no dispositivo que irá utilizá-la. Veja na tabela abaixo mais detalhes sobre a configuração exigida por cada tipo de impressora:

<table>
	<tr>
		<th>Tipo de Impressora</th>
		<th>Descrição</th>
	</tr>
	<tr>
		<td><b>Android</b></td>
		<td>
			Impressora configurada nativamente no dispositivo Android. Utilize esse tipo caso a impressora desejada esteja configurada diretamente no aparelho (podendo ser utilizada por outros aplicativos instalados no aparelho).<br><br>
			<em>Nenhuma configuração adicional é exigida para esse tipo de impressora</em>
		</td>
	</tr>
	<tr>
		<td><b>Rede</b></td>
		<td>
			Impressora compartilhada em rede e instalada na mesma rede local em que o dispositivo está conectado. Utilize esse tipo caso a impressora seja uma impressora de rede ou wifi.<br><br>
			<b>Configurações adicionais exigidas:</b><br>
			<b>IP:</b> Endereço IP da impressora na rede local a qual o dispositivo está conectado<br>
			<b>Porta:</b> Porta de comunicação utilizada para enviar dados para impressora
		</td>
	</tr>
	<tr>
		<td><b>Bluetooth</b></td>
		<td>
			Impressora dedicada ao dispositivo android e conectada a ele através da interface Bluetooth. Utilize esse tipo caso a impressora seja uma impressora Bluetooth.<br><br>
			<b>Configurações adicionais exigidas:</b><br>
			<b>Modelo:</b> Modelo da impressora bluetooth
		</td>
	</tr>
</table>

# Alterando Impressora existente

1. Selecione a impressora que deseja alterar (_Ver [Listando Impressoras do estacionamento](#listando-impressoras-do-estacionamento) para mais detalhes_)
2. Será exibido o formulário de edição da impressora
3. Preencha os campos desejados do formulário e clique no botão **Salvar** (_Ver [Tipos de Impressoras suportadas](#tipos-de-impressoras-suportadas) para mais detalhes_)
4. Pronto! As configurações da Impressora foram alterados e salvos.

# Removendo Impressora existente

`Funcionalidade indisponível no momento`
